'use strict';
const log = console.log;
// ========================= Переменные =========================
// массив для хранения id строк
const idList = [];
// для обработки нажатий внутри контейнера
const ulContainer = document.querySelector('.js-ul-list');
// TextBox
const inputText = document.querySelector('.js-textbox');
// Кнопки
const addButton = document.querySelector('.js-btn-add');
const updButton = document.querySelector('.js-btn-upd');
const resetBtn = document.querySelector('.js-btn-reset');
// Временное хранилище элемента
let tmpElem = null;

// ========================= Функции =========================
// Чтение поля
const getText = () => inputText.value;

// Менеджер id для строк
const getId = (idList) => {
  let itemsCount = 0;
  while (idList.includes(itemsCount)) {
    itemsCount++;
  }
  idList.push(itemsCount);
  return itemsCount;
};

// Деактивация кнопки добавить
const addButtonEn = () => {
  if (addButton.classList.contains('js-btn-add--hidden')) return;
  if (inputText.value == '') {
    addButton.disabled = true;
    addButton.classList.add('js-btn-add--disabled');
  } else {
    addButton.disabled = false;
    addButton.classList.remove('js-btn-add--disabled');
  }
};
// Деактивация кнопки обновить
const updButtonEn = () => {
  if (updButton.classList.contains('js-btn-upd--hidden')) return;
  if (inputText.value == '') {
    updButton.disabled = true;
    updButton.classList.add('js-btn-upd--disabled');
    resetBtn.classList.add('js-btn-reset--active');
  } else {
    updButton.disabled = false;
    updButton.classList.remove('js-btn-upd--disabled');
    resetBtn.classList.remove('js-btn-reset--active');
  }
};

// Спрятать пустой список задач
const checkListForEmptiness = () => {
  if (ulContainer.children.length === 0)
    ulContainer.classList.toggle('js-list--empty');
};

// ========================= Алгоритм =========================
// Деактивация кнопки Добавить
inputText.addEventListener('input', addButtonEn);
addButtonEn();

// Деактивация кнопки Обновить
inputText.addEventListener('input', updButtonEn);
updButtonEn();

// Checkbox зачеркнуть
ulContainer.addEventListener('change', (event) => {
  if (!event.target.classList.contains('js-cb')) return;
  event.target.nextElementSibling.classList.toggle('js-cb--checked');
});
// Удаление строк
ulContainer.addEventListener('click', (event) => {
  if (!event.target.classList.contains('js-item__close')) return;
  event.target.parentElement.remove();
  // удаление id строки из массива idList
  const idVar = event.target.getAttribute('id');
  const val = idVar
    .split('')
    .splice(idVar.split('').indexOf('_') + 1)
    .join('');
  idList.splice(idList.indexOf(Number(val)), 1);
  checkListForEmptiness();
});

// Редактирование строки
ulContainer.addEventListener('click', (event) => {
  if (!event.target.classList.contains('js-item__label')) return;
  inputText.value = event.target.textContent;
  inputText.focus();
  inputText.select();
  addButton.classList.add('js-btn-add--hidden');
  updButton.classList.remove('js-btn-upd--hidden');
  if (tmpElem !== null) tmpElem.parentElement.classList.remove('js-li--active');
  tmpElem = event.target;
  tmpElem.parentElement.classList.add('js-li--active');
});

// Кнопка Обновить
updButton.addEventListener('click', () => {
  const textFromTextBox = getText(); // Чтение поля
  tmpElem.textContent = textFromTextBox;
  // Очистка поля
  inputText.value = '';
  addButton.classList.toggle('js-btn-add--hidden');
  updButton.classList.toggle('js-btn-upd--hidden');
  tmpElem.parentElement.classList.remove('js-li--active');
  tmpElem = null;
});

// Кнопка Добавить
addButton.addEventListener('click', () => {
  checkListForEmptiness();
  const textFromTextBox = getText(); // Чтение textbox
  const liLast = document.createElement('li');
  liLast.className = 'js-list__item';
  liLast.insertAdjacentHTML(
    'beforeend',
    '<input class="js-cb" type="checkbox" name="element" /><label class="js-item__label"></label><span class="js-item__close">x</span>'
  );
  ulContainer.append(liLast);

  const currentId = getId(idList);
  // Для новой строки добавляем id в chechbox
  const elementsCB = document.querySelectorAll('.js-cb');
  for (let elem of elementsCB) {
    if (!elem.hasAttribute('id')) elem.id = `cb_${currentId}`;
  }
  // Для новой строки добавляем id в label и текст из поля
  const elementsLabel = document.querySelectorAll('.js-item__label');
  for (let elem of elementsLabel) {
    if (!elem.hasAttribute('id')) {
      elem.setAttribute('id', `lab_${currentId}`);
      elem.innerHTML = textFromTextBox;
    }
  }
  // Для новой строки добавляем id в span
  const elementsClose = document.querySelectorAll('.js-item__close');
  for (let elem of elementsClose) {
    if (!elem.hasAttribute('id')) elem.id = `close_${currentId}`;
  }
  // Очистка поля
  inputText.value = '';
  addButtonEn();
});

// Кнопка Отмены
resetBtn.addEventListener('click', () => {
  addButton.disabled = true;
  addButton.classList.add('js-btn-add--disabled');
  addButton.classList.remove('js-btn-add--hidden');
  updButton.classList.add('js-btn-upd--hidden');
  resetBtn.classList.remove('js-btn-reset--active');
  tmpElem.parentElement.classList.remove('js-li--active');
  tmpElem = null;
});
